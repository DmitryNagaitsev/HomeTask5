﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace HomeTask5.Controllers
{
    [Route("api/[controller]")]
    public class LibraryController : Controller
    {
        private static Person _currentPerson;
        private static ICollection<Book> _requestedBooks;
        private static List<Book> _returnedBooks;
        private static readonly ICollection<LibraryItem> Library = new List<LibraryItem>()
        {
            new LibraryItem { OnHand = true, Book = new Book { Id = 1, Author = "Loyd A. G.", BookType = Book.BookTypes.Magazine, Name = "Guide for fats", Year = 322}},
            new LibraryItem { OnHand = false, Book = new Book { Id = 2, Author = "Author1", BookType = Book.BookTypes.Scientific, Name = "Book1"}},
            new LibraryItem { OnHand = true, Book = new Book { Id = 3, Author = "Author2", BookType = Book.BookTypes.Scientific, Name = "Book2"}},
            new LibraryItem { OnHand = false, Book = new Book { Id = 4, Author = "Author3", BookType = Book.BookTypes.Scientific, Name = "Book3"}},
            new LibraryItem { OnHand = false, Book = new Book { Id = 5, Author = "Author4", BookType = Book.BookTypes.Scientific, Name = "Book4"}},
        };
        private static readonly Dictionary<int, ICollection<Order>> Orders = new Dictionary<int, ICollection<Order>>()
        {
            {1, new List<Order>
                {
                    new Order { OrderCreateTime = DateTime.Now.AddMonths(-2), BookId = 1 }
                }
            },
            {2, new List<Order>()
            {
                new Order { OrderCreateTime = DateTime.Now.AddMonths(-2), BookId = 3 }
            } }
        };

        [HttpPost]
        [Route("join")]
        public void JoinLibrary([FromBody]Person person)
        {
            _currentPerson = person;
            _requestedBooks = new List<Book>();
            _returnedBooks = new List<Book>();
            Console.WriteLine($"Hello, {person.FIO}! You have {Orders[_currentPerson.Id].Count} books");
        }

        [HttpPost]
        [Route("return")]
        public void ReturnBooks([FromBody]List<Book> books)
        {
            _returnedBooks.AddRange(books);
        }

        [HttpGet]
        [Route("books")]
        public ICollection<string> GetBooks()
        {
            return Library.Select(x => x.Book.Name).ToList();
        }

        [HttpPost]
        [Route("apply")]
        public ICollection<Book> Apply()
        {
            foreach (var returned in _returnedBooks)
            {
                Orders[_currentPerson.Id].Remove(Orders[_currentPerson.Id]
                    .Single(x => x.BookId.Equals(returned.Id)));
                Library.Single(x => x.Book.Id == returned.Id).OnHand = false;
            }
            Console.WriteLine($"{_returnedBooks.Count} books returned.");

            foreach (var request in _requestedBooks)
            {
                Library.Single(x => x.Book.Id == request.Id).OnHand = true;
                Orders[_currentPerson.Id].Add(
                    new Order { OrderCreateTime = DateTime.Now, BookId = request.Id });
            }
            Console.WriteLine($"{_requestedBooks.Count} books taken.");
            Console.WriteLine($"Now you have {Orders[_currentPerson.Id].Count} books.");
            Console.WriteLine($"Bye, {_currentPerson.FIO}!");
            Console.WriteLine();
            return _requestedBooks;
        }

        [HttpPost]
        [Route("request")]
        public void RequestBook([FromBody]string sId)
        {
            var id = int.Parse(sId);
            _requestedBooks.Add(Library.Single(x => x.Book.Id == id).Book);
        }

        [HttpPost]
        [Route("leave")]
        public void Leave()
        {
            Console.WriteLine($"Bye, {_currentPerson.FIO}!");
        }
    }

    public class Book
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Author { get; set; }

        public int Year { get; set; }

        public BookTypes BookType { get; set; }

        public enum BookTypes
        {
            /// <summary>
            /// Журнал
            /// </summary>
            Magazine,
            /// <summary>
            /// Художественная книга
            /// </summary>
            Fiction,
            /// <summary>
            /// Научная
            /// </summary>
            Scientific
        }
    }

    public class Person
    {
        public int Id { get; set; }
        // ReSharper disable once InconsistentNaming
        public string FIO { get; set; }
    }

    public class LibraryItem
    {
        public bool OnHand { get; set; }

        public Book Book { get; set; }
    }

    public class Order
    {
        public DateTime OrderCreateTime { get; set; }

        public int BookId { get; set; }
    }
}
